import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FormsModule} from '@angular/forms';
import {ChartModule} from "angular-highcharts";
import {MagnetSensorComponent} from "./magnet-sensor/magnet-sensor.component";
import {GyroSensorComponent} from "./gyro-sensor/gyro-sensor.component";
import {SensorComboComponent} from "./sensor-combo/sensor-combo.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ChartModule
    ],
    declarations: [
        MagnetSensorComponent,
        GyroSensorComponent,
        SensorComboComponent
    ],
    exports: [
        MagnetSensorComponent,
        GyroSensorComponent,
        SensorComboComponent
    ]
})
export class ComponentsModule
{
}
