
export interface Gate
{
    label: string;
    sensor: number;
    key: number;
    rgbString;
    percentError: number;
    frozen: boolean;
    solved: boolean;
}

export function numberFailingGates(gates: Gate[]): number
{
    let failingGates = gates
        .map(gate => isGatePassing(gate))
        .filter(passing => !passing);

    return failingGates.length;
}

export function getGatePercentage(gate: Gate): number
{
    return Math.abs(gate.sensor - gate.key) / 20;
}

export function isGatePassing(gate: Gate): boolean
{
    return getGatePercentage(gate) < .1;
}
