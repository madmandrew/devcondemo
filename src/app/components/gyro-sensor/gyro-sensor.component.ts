import {Component, OnDestroy, OnInit} from '@angular/core';
import {Sensors, TYPE_SENSOR} from '@ionic-native/sensors/ngx';
import {Subject, timer} from 'rxjs';
import {Gate, getGatePercentage, isGatePassing, numberFailingGates} from './Gate';
import {takeUntil} from "rxjs/operators";

@Component({
    selector: 'app-gyro-sensor-demo',
    templateUrl: './gyro-sensor.component.html',
    styleUrls: ['./gyro-sensor.component.scss'],
})
export class GyroSensorComponent implements OnInit, OnDestroy
{
    gates: Gate[] = [
        {
            label: "Gate One",
            sensor: 0,
            key: Math.floor(Math.random() * 20),
            rgbString: '255, 0',
            percentError: 0,
            frozen: false,
            solved: false
        },
        {
            label: "Gate Two",
            sensor: 0,
            key: Math.floor(Math.random() * 20),
            rgbString: '255, 0',
            percentError: 0,
            frozen: false,
            solved: false
        },
        {
            label: "Gate Three",
            sensor: 0,
            key: Math.floor(Math.random() * 20),
            rgbString: '255, 0',
            percentError: 0,
            frozen: false,
            solved: false
        }
    ];

    failingGates: number = 3;
    private destroySubject: Subject<void> = new Subject();

    constructor(private sensors: Sensors)
    {
    }

    ngOnInit()
    {
        this.initSensor();
    }

    ngOnDestroy()
    {
        this.destroySubject.next();
        this.sensors.disableSensor();
    }

    private async initSensor()
    {
        //<editor-fold desc="Demo step 1">
        this.sensors.enableSensor(TYPE_SENSOR.GRAVITY);

        timer(50, 100)
            .pipe(takeUntil(this.destroySubject))
            .subscribe(() =>
            {
                this.sensors.getState().then(result => {
                    this.handleGates(result);
                });
            });
        //</editor-fold
    }

    private handleGates(results: number[])
    {
        //<editor-fold desc="Demo Step 2">
        let index = 0;
        for (let gate of this.gates)
        {
            if (!gate.frozen)
            {
                gate.sensor = results[index] + 10;
                gate.percentError = getGatePercentage(gate);
                gate.solved = isGatePassing(gate);
                gate.rgbString = this.getRbgString(gate.sensor, gate.key);
            }
            index += 1;
        }
        this.failingGates = numberFailingGates(this.gates);
        //</editor-fold>
    }

    private getRbgString(sensor: number, key: number)
    {
        let percent = (Math.abs(sensor - key) / 20);
        return `${255 * percent}, ${150 * (1 - percent)}`;
    }
}
