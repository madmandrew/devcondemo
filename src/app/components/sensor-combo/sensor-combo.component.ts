import {Component, OnDestroy, OnInit} from '@angular/core';
import {Sensors, TYPE_SENSOR} from '@ionic-native/sensors/ngx';
import {Subject, timer} from 'rxjs';
import {Flashlight} from '@ionic-native/flashlight/ngx';
import {BackgroundMode} from '@ionic-native/background-mode/ngx';
import {takeUntil} from "rxjs/operators";

@Component({
    selector: 'app-flashlight-combo-demo',
    templateUrl: './sensor-combo.component.html',
    styleUrls: ['./sensor-combo.component.scss'],
})
export class SensorComboComponent implements OnInit, OnDestroy
{
    lightOn: boolean = false;
    lightbaseline: number = 10;

    backgroundEnabled: boolean = false;
    private destroySubject: Subject<void> = new Subject();

    constructor(private sensor: Sensors, private flashlight: Flashlight, private backgroundMode: BackgroundMode)
    {
    }

    ngOnInit()
    {
        this.sensor.enableSensor(TYPE_SENSOR.LIGHT);

        timer(50, 100)
            .pipe(takeUntil(this.destroySubject))
            .subscribe(() => {
                this.sensor.getState().then(result => this.handleLightSensor(result[0]));
            });

        this.backgroundMode.setDefaults({
            title: 'Checking for light...',
            text: ''
        })
    }

    ngOnDestroy()
    {
        this.destroySubject.next();
        this.sensor.disableSensor();
    }

    toggleBackgroundMode()
    {
        this.backgroundEnabled = !this.backgroundMode.isEnabled();
        this.backgroundMode.setEnabled(this.backgroundEnabled);
    }

    zeroLightSensor()
    {
        this.sensor.getState().then(result => this.lightbaseline = result[0]);
    }

    private handleLightSensor(result: number)
    {
        console.log("Light Sensor reading: ", result);
        if (result < this.lightbaseline && !this.lightOn)
        {
            this.flashlight.switchOn();
            this.lightOn = true;
            this.backgroundMode.wakeUp();
        }
        else if (result >= this.lightbaseline && this.lightOn)
        {
            this.flashlight.switchOff();
            this.lightOn = false;
        }

    }
}
