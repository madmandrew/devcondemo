export const MagnetChartOptions = {
    chart: {
        height: 200,
        width: 200,
        margin: 0,
        type: 'scatter3d',
        animation: false,
        options3d: {
            enabled: true,
            alpha: 20,
            beta: 50,
            depth: 600,
            viewDistance: 3,
            fitToPlot: true,
            frame: {
                bottom: {size: 1, color: 'rgba(0,0,0,0.02)'},
                back: {size: 1, color: 'rgba(0,0,0,0.04)'},
                side: {size: 1, color: 'rgba(0,0,0,0.06)'}
            }
        }
    },
    title: {
        text: 'Magnet Sensor'
    },
    plotOptions: {
        scatter: {
            width: 500,
            height: 500,
            depth: 500
        }
    },
    yAxis: {
        min: -500,
        max: 500,
        title: null
    },
    xAxis: {
        min: -500,
        max: 500,
        gridLineWidth: 1
    },
    zAxis: {
        min: -500,
        max: 500,
        showFirstLabel: false
    },
    legend: {
        enabled: false
    },
    series: [{
        name: 'Reading',
        colorByPoint: true,
        data: []
    }]
};
