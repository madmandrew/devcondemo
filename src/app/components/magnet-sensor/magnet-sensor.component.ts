import {Component, OnDestroy, OnInit} from '@angular/core';
import {Sensors, TYPE_SENSOR} from '@ionic-native/sensors/ngx';
import {Subject, timer} from 'rxjs';
import {Chart} from 'angular-highcharts';
import {MagnetChartOptions} from './ChartOptions';
import {takeUntil} from "rxjs/operators";

@Component({
    selector: 'app-magnet-sensor-demo',
    templateUrl: './magnet-sensor.component.html',
    styleUrls: ['./magnet-sensor.component.scss'],
})
export class MagnetSensorComponent implements OnInit, OnDestroy {
    maxLength: number = 200;

    chart;
    private destroySubject: Subject<void> = new Subject();

    constructor(private sensors: Sensors) {
    }

    ngOnInit() {
        this.initChart();
        this.initMagnetSensor();
    }

    ngOnDestroy()
    {
        this.destroySubject.next();
        this.sensors.disableSensor();
    }

    private initChart() {
        this.chart = new Chart(MagnetChartOptions as any);
    }

    private initMagnetSensor() {
        this.sensors.enableSensor(TYPE_SENSOR.MAGNETIC_FIELD);

        timer(50, 100)
            .pipe(takeUntil(this.destroySubject))
            .subscribe(() => {
                this.sensors.getState().then(result => {
                    this.updateGraphPoints(result);
                });
            });
    }

    private updateGraphPoints(result: number[]) {
        const numPoints = this.chart.options.series[0].data.length;
        console.log('Adding point: ', result, numPoints);
        if (numPoints > this.maxLength)
        {
            this.chart.removePoint(0);
        }
        this.chart.addPoint(result);
    }

    public clearPoints()
    {
        this.chart.removeSeries(0);
        this.chart.addSeries({
            name: 'Reading',
            colorByPoint: true,
            data: []
        });
    }
}
