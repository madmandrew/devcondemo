import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {Sensors} from '@ionic-native/sensors/ngx';
import highcharts3D from 'highcharts/highcharts-3d.src.js';
import {ChartModule, HIGHCHARTS_MODULES} from 'angular-highcharts';
import {Flashlight} from '@ionic-native/flashlight/ngx';
import {BackgroundMode} from '@ionic-native/background-mode/ngx';

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        ChartModule
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},

        Sensors,
        Flashlight,
        BackgroundMode,
        { provide: HIGHCHARTS_MODULES, useFactory: () => [ highcharts3D ] }
    ],
    bootstrap: [AppComponent]
})
export class AppModule
{
}
