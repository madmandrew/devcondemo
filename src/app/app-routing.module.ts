import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'gyro-sensor', loadChildren: () => import('./gyro-sensor/gyro-sensor.module').then(m => m.GyroSensorPageModule)},
  { path: 'magnet-sensor', loadChildren: () => import('./magnet-sensor/magnet-sensor.module').then(m => m.MagnetSensorPageModule) },
  { path: 'flashlight-combo', loadChildren: () => import('./flashlight-combo/flashlight-combo.module').then(m => m.FlashlightComboPageModule) },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
